module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable("UserEmails", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			userId: {
				type: Sequelize.INTEGER,
				references: {
					model: "Users",
					key: "id"
				}
			},
			emailId: {
				type: Sequelize.INTEGER,
				references: {
					model: "Emails",
					key: "id"
				}
			},
			createdAt: {
				type: Sequelize.DATE
			},
			updatedAt: {
				type: Sequelize.DATE
			}
		});
	},
	down: (queryInterface, Sequelize) => queryInterface.dropTable("UserEmails"),
};