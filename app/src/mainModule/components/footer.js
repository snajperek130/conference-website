import React from "react";

const Footer = () => (
    <div id="footer">
        <div className="container">
            <div className="col-md-4 col-sm-6 col-xs-12">
                <h2 className="footer-title">Pomoc</h2>
                <p>Do kiedy mogę się zapisać?</p>
                <p>Jak będzie wyglądać proces zxapisu?</p>
                <p>Kiedy mogę spodziewać się wyników zapisu?</p>
            </div>
            <div className="col-md-4 col-sm-6 col-xs-12">
                <h2 className="footer-title">Pomocne informacje</h2>
                <p>Znajdź miejsce na mapie</p>
                <p>Lista zapisów</p>
                <p>Więcej informacji na temat konferencji</p>
            </div>
            <div className="col-md-4 col-sm-6 col-xs-12">
                <h2 className="footer-title">Znajdź nas również na:</h2>
                <i className="fa fa-twitter-square" aria-hidden="true"/>
                <i className="fa fa-facebook-square"/>
            </div>
        </div>
    </div>
);

export default Footer;
