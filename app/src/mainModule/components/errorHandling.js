import React from "react";
import PropTypes from "prop-types";

const ErrorDisplay = ({message}) => (
    <div>
        <p>{message}</p>
    </div>
);

export default ErrorDisplay;

ErrorDisplay.propTypes = {message: PropTypes.string};