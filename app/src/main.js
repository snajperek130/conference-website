import React from "react";
import "./mainModule/assets/style.css";
import {Route, Switch} from "react-router-dom";
import MainPage from "./mainModule/containers/main-page";
import Informations from "./mainModule/components/informations";
import Program from "./mainModule/containers/program";
import Members from "./mainModule/containers/members";
import Committee from "./mainModule/components/committee";
import ToAutor from "./mainModule/components/to-autor";
import Fees from "./mainModule/components/fees";
import Contact from "./mainModule/components/contact";
import AdminLogin from "./mainModule/containers/admin-login";
import Account from "./mainModule/containers/account";
import AdminPanel from "./mainModule/containers/admin";
import ApplicationForm from "./mainModule/containers/applicationForm";

const Main = () => (
	<Switch>
		<Route exact path='/' render={props => <MainPage {...props} />}/>
		<Route path='/informations' render={props => <Informations {...props} />}/>
		<Route path='/program' render={props => <Program {...props} />}/>
		<Route path='/members' render={props => <Members {...props} />}/>
		<Route path='/committee' render={props => <Committee {...props} />}/>
		<Route path='/to-autor' render={props => <ToAutor {...props} />}/>
		<Route path='/payments' render={props => <Fees {...props} />}/>
		<Route path='/contact' render={props => <Contact {...props} />}/>
		<Route path='/register-login' render={props => <AdminLogin {...props} />}/>
        <Route path='/account' render={props => <Account {...props} />}/>
        <Route path='/admin' render={props => <AdminPanel {...props} />}/>
        <Route path='/admin' render={props => <AdminPanel {...props} />}/>
        <Route path='/form' render={props => <ApplicationForm {...props} />}/>
	</Switch>
);

export default Main;