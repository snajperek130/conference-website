export const UserAPI = {
    register: "/api/register",
    login: "/api/login",
    verify: "/api/verify",
    getUser: "/api/user",
    getUsers: "/api/users",
    create: "/api/create-user",
    update: "/api/update-user",
    updatePassword: "/api/update-password",
    delete: "/api/delete-user:id"
};

export const AttachmentAPI = {
    upload: "/api/upload-file",
    delete: "/api/delete-file",
    getFiles: "/api/get-files"
};

export const ProgramAPI = {
    getProgram: "/api/program",
    getWholeProgram: "/api/all-program",
    createProgram: "/api/create-program"
};