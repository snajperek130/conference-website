import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {getFiles, removeFile, uploadFile} from "../../redux/actions/attachemntActions";
import {getProfile, updatePassword, updateProfile} from "../../redux/actions/userActions";
import {bindActionCreators} from "redux";
import ErrorDisplay from "../components/errorHandling";
import Tab from "react-tabs/esm/components/Tab";
import Tabs from "react-tabs/esm/components/Tabs";
import TabList from "react-tabs/esm/components/TabList";
import TabPanel from "react-tabs/esm/components/TabPanel";
import Dropzone from "react-dropzone";
import * as _ from "lodash";

class Account extends React.Component {

    constructor() {
        super();
        this.state = {
            accepted: [],
            rejected: []
        };
        this.handleUpdateProfile = this.handleUpdateProfile.bind(this);
        this.sendFile = this.sendFile.bind(this);
        this.handleUpdatePassword = this.handleUpdatePassword.bind(this);
        // this.handleUploadFile = this.handleUploadFile.bind(this);
        // this.handleDeleteFile = this.handleDeleteFile.bind(this);
    }

    componentDidMount() {
        // console.log(this.props.getProfile());
    }

    handleUpdatePassword(e) {
        e.preventDefault();
        this.props.updatePassword({password: this.password.value});
    }

    dropAction({accepted, rejected}) {
        this.setState({
            accepted: accepted,
            rejected: rejected
        });
    }

    sendFile() {
        let files = new FormData();
        _.forEach(this.state.accepted, acceptedFile => {
            files.append("file", acceptedFile, acceptedFile.name);
        });
        console.log(files.getAll("file"));
        this.props.uploadFile(files);
    }

    handleUpdateProfile(e) {
        e.preventDefault();
        const user = {
            firstname: this.firstName.value,
            lastname: this.lastName.value,
            degreeTitle: this.degreeTitle.value,
            university: this.university.value,
            dateOfBirth: this.dateOfBirth.value
        };

        this.props.updateProfile(user);
    }

    downloadFile(file) {
        const link = document.createElement("a");
        link.href = file.url;
        link.download = file.name;
        link.click();
    }

    deleteFileLocally() {
    }

    deleteFile(data) {
        console.log(this.props.deleteFile(data));
    }

    render() {
        return (
            <main className="down">
                <header className="header-section">
                    <div className="header-title">Komitety</div>
                </header>
                {(() => {
                    if (this.props.errorMessage.error) {
                        return (<ErrorDisplay message={this.props.errorMessage.error}/>);
                    }
                })()}
                <section className="members-table-section">
                    <div className="container">
                        <div className="push-md-1 col-md-10 col-pull-1">
                            <Tabs>
                                <TabList>
                                    <Tab>Profil</Tab>
                                    <Tab>Zmiana hasła</Tab>
                                    <Tab>Zapis plików</Tab>
                                </TabList>

                                <TabPanel>
                                    <form id="tab">
                                        <div className="form-group">
                                            <label>Imie:</label>
                                            <input ref={(input) => { this.firstName = input;}} name="firstname" type="text" className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Nazwisko:</label>
                                            <input ref={(input) => { this.lastName = input;}} name="surname" type="text" className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Tutuł naukowy:</label>
                                            <input ref={(input) => { this.degreeTitle = input;}} name="degreeTitle" type="text" className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Uniwersytet:</label>
                                            <input ref={(input) => { this.university = input;}} name="university" type="text" className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Data urodzenia:</label>
                                            <input ref={(input) => { this.dateOfBirth = input;}} name="university" type="text" className="form-control"/>
                                        </div>
                                        <div onClick={this.handleUpdateProfile}>
                                            <button className="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </TabPanel>

                                <TabPanel>
                                    <form>
                                        <div className="form-group">
                                            <label>Password:</label>
                                            <input ref={(input) => { this.password = input;}} type="password" className="form-control"/>
                                        </div>
                                        <div onClick={this.handleUpdatePassword}>
                                            <button className="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                </TabPanel>
                                <TabPanel>
                                    <div className="dropzone">
                                        <Dropzone accept=".doc, .pdf"
                                            onDrop={(accepted, rejected) => {
                                            this.dropAction({accepted, rejected});
                                        }}>
                                            <p>Try dropping some files here, or click to select files to upload.</p>
                                            <p>Only *.jpeg and *.png images will be accepted</p>
                                        </Dropzone>
                                    </div>
                                    <aside>
                                        <h2>Pliki do zapisu</h2>
                                        <ul>
                                            {
                                                this.state.accepted.map(f =>
                                                    <li key={f.name}>{f.name} - {f.size} bytes
                                                    </li>)
                                            }
                                        </ul>
                                        <h2>Pliki, które nie będą zapisane</h2>
                                        <ul>
                                            {
                                                this.state.rejected.map(f =>
                                                    <li key={f.name}>{f.name} - {f.size} bytes
                                                    </li>)
                                            }
                                        </ul>
                                    </aside>
                                    <button onClick={this.sendFile}>SEND</button>
                                </TabPanel>

                            </Tabs>
                        </div>
                    </div>
                </section>
            </main>
        );
    }
}

function mapStateToProps(state) {
    return {
        errorMessage: state.error,
        user: state.user,
        attachments: state.attachment
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProfile: getProfile,
        updateProfile: updateProfile,
        updatePassword: updatePassword,
        uploadFile: uploadFile,
        getFiles: getFiles,
        deleteFile: removeFile,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Account);

Account.propTypes = {getProfile: PropTypes.func};
Account.propTypes = {updateProfile: PropTypes.func};
Account.propTypes = {updatePassword: PropTypes.func};
Account.propTypes = {uploadFile: PropTypes.func};
Account.propTypes = {removeFile: PropTypes.func};
Account.propTypes = {getFiles: PropTypes.func};
Account.propTypes = {deleteFile: PropTypes.func};
Account.propTypes = {errorMessage: PropTypes.string};