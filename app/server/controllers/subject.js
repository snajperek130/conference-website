const Subject = require("../models").Subject;
const UserSubject = require("../models").UserSubject;

module.exports = {
	getAllSubjects() {
		return Subject.all();
	},
	update(req, res) {
		return Subject
			.find({
				where: {
					id: req.body.id
				},
			})
			.then(subject => {
				if (!subject) {
					return res.status(404).send({
						message: "Subject Not Found",
					});
				}

				return subject
					.update({
						title: req.body.title,
						description: req.body.description,
						date: req.body.date
					})
					.then(subject => res.status(200).send(subject))
					.catch(error => res.status(400).send(error));
			})
			.catch(error => res.status(400).send(error));
	},
    async create(req, res) {
        return Subject
            .create({
                title: req.body.title,
                description: req.body.description,
                date: req.body.date
            });
    },
};