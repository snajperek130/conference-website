import React from "react";
import {Col} from "react-bootstrap";
import $ from "jquery";
import ScrollReveal from "scrollreveal";

class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.renderTimeline();
    }

    render() {
        return (
            <main>
                <header className="main-header-section">
					<span className="main-header-container">
						<h1>IX Konferencja Naukowa</h1>
						<h1>"Symbioza Techniki i informatyki"</h1>
						<button type="button" className="btn btn-default button">Zapisz się już dzisiaj!</button>
					</span>
                </header>

                <section className="about-section">
                    <div className="container">
                        <div className="row">
                            <Col md={6} sm={12} xs={12}>
                                <img alt="about" className="img-responsive"
                                     src={require("../assets/photos/about.jpg")}/>
                            </Col>

                            <Col md={6} sm={12} xs={12}>
                                <div className="about-text-wrapper">
                                    <p>textttttttttttttttttttttttttt tttttttttttttttttttttt tttttttttttttttttttttt</p>
                                    <p>smaler textttttttttttttttttt tttttttttttttttttttttttttt ttttttttttttttttttt</p>
                                </div>
                            </Col>
                        </div>
                    </div>
                </section>

                <section className="how-it-works-section">
                    <div className="container">
                        <div className="row">
                            <p>Jak to działa?</p>
                            <p>Zapisz się na konferencję, dzięki czemu będziesz mógł uczestniczyć w tym ciekawym
                                wydarzeniu.
                                Powodzenia :).</p>
                            <div className="step">
                                <img alt="about" className="img-responsive arrow"
                                     src={require("../assets/photos/arrow.png")}/>
                                <p className="step-count">Krok 1</p>
                                <p className="step-text">Zarejestruj się w celu umożliwenia zapisania się na
                                    wydarzenie.</p>
                            </div>

                            <div className="step">
                                <img alt="about" className="img-responsive arrow"
                                     src={require("../assets/photos/arrow.png")}/>
                                <p className="step-count">Krok 2</p>
                                <p className="step-text">Czekaj na potwierdzenie, które wysyłamy na pocztę enual.
                                    Upewnij
                                    się, czy nasza
                                    wiadomość nie trafiła do folderu spam.
                                </p>
                            </div>

                            <div className="step">
                                <img alt="about" className="img-responsive arrow"
                                     src={require("../assets/photos/arrow.png")}/>
                                <p className="step-count">Krok 3</p>
                                <p className="step-text">Po pomyślnym potwierdzeniu rezerwacji należy uiścić opłatę.
                                    Wszelkie
                                    dane potrzebne do zrealizowania przelewu zostanąpodane w naszym profilu.
                                </p>
                            </div>

                            <div className="step">
                                <img alt="about" className="img-responsive arrow"
                                     src={require("../assets/photos/arrow.png")}/>
                                <p className="step-count">Krok 4</p>
                                <p className="step-text">Ostatnim krokiem jest sprawdzenie swojej obecności w zakładce
                                    "Uczestnicy".
                                    Lista jest aktualizowana automatycznie, więc po potwierdzeniu opłaty Zistaniecie
                                    wpisani
                                    na listę.
                                </p>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="timeline-section">
                    <header>
                        <div className="container text-center">
                            <h1>Planowanie</h1>
                            <p>Tak bedzie wyglądał zapis</p>
                        </div>
                    </header>

                    <section className="timeline">
                        <div className="container">
                            <div className="timeline-item">
                                <div className="timeline-img"/>

                                <div className="timeline-content js--fadeInLeft">
                                    <h2>Title</h2>
                                    <div className="date">1 MAY 2016</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ipsa ratione
                                        omnis alias cupiditate saepe atque totam aperiam sed nulla voluptatem recusandae
                                        dolor, nostrum excepturi amet in dolores. Alias, ullam.</p>
                                    <a className="bnt-more" href="javascript:void(0)">More</a>
                                </div>
                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content timeline-card js--fadeInRight">
                                    <div className="timeline-img-header">
                                        <h2>Card Title</h2>
                                    </div>
                                    <div className="date">25 MAY 2016</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ipsa ratione
                                        omnis alias cupiditate saepe atque totam aperiam sed nulla voluptatem recusandae
                                        dolor, nostrum excepturi amet in dolores. Alias, ullam.</p>
                                    <a className="bnt-more" href="javascript:void(0)">More</a>
                                </div>

                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content js--fadeInLeft">
                                    <div className="date">3 JUN 2016</div>
                                    <h2>Quote</h2>
                                    <blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta
                                        explicabo debitis omnis dolor iste fugit totam quasi inventore!
                                    </blockquote>
                                </div>
                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content js--fadeInRight">
                                    <h2>Title</h2>
                                    <div className="date">22 JUN 2016</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ipsa ratione
                                        omnis alias cupiditate saepe atque totam aperiam sed nulla voluptatem recusandae
                                        dolor, nostrum excepturi amet in dolores. Alias, ullam.</p>
                                    <a className="bnt-more" href="javascript:void(0)">More</a>
                                </div>
                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content timeline-card js--fadeInLeft">
                                    <div className="timeline-img-header">
                                        <h2>Card Title</h2>
                                    </div>
                                    <div className="date">10 JULY 2016</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ipsa ratione
                                        omnis alias cupiditate saepe atque totam aperiam sed nulla voluptatem recusandae
                                        dolor, nostrum excepturi amet in dolores. Alias, ullam.</p>
                                    <a className="bnt-more" href="javascript:void(0)">More</a>
                                </div>
                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content timeline-card js--fadeInRight">
                                    <div className="timeline-img-header">
                                        <h2>Card Title</h2>
                                    </div>
                                    <div className="date">30 JULY 2016</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ipsa ratione
                                        omnis alias cupiditate saepe atque totam aperiam sed nulla voluptatem recusandae
                                        dolor, nostrum excepturi amet in dolores. Alias, ullam.</p>
                                    <a className="bnt-more" href="javascript:void(0)">More</a>
                                </div>
                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content js--fadeInLeft">
                                    <div className="date">5 AUG 2016</div>
                                    <h2>Quote</h2>
                                    <blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta
                                        explicabo debitis omnis dolor iste fugit totam quasi inventore!
                                    </blockquote>
                                </div>
                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content timeline-card js--fadeInRight">
                                    <div className="timeline-img-header">
                                        <h2>Card Title</h2>
                                    </div>
                                    <div className="date">19 AUG 2016</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ipsa ratione
                                        omnis alias cupiditate saepe atque totam aperiam sed nulla voluptatem recusandae
                                        dolor, nostrum excepturi amet in dolores. Alias, ullam.</p>
                                    <a className="bnt-more" href="javascript:void(0)">More</a>
                                </div>
                            </div>

                            <div className="timeline-item">

                                <div className="timeline-img"/>

                                <div className="timeline-content js--fadeInLeft">
                                    <div className="date">1 SEP 2016</div>
                                    <h2>Title</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ipsa ratione
                                        omnis alias cupiditate saepe atque totam aperiam sed nulla voluptatem recusandae
                                        dolor, nostrum excepturi amet in dolores. Alias, ullam.</p>
                                    <a className="bnt-more" href="javascript:void(0)">More</a>
                                </div>
                            </div>


                        </div>
                    </section>

                </section>

                <section className="interested-section">
                    <div className="fluid-container center-elements">
                        <div className="row">
                            <h2>Zainteresowany? Nie czekaj!</h2>
                            <p>Kliknij w powyższy przycisk i zapisz się na to wydarzenie, napewno nie pożałujes podjętej
                                decyzji.</p>
                            <button type="button" className="btn btn-default button">Zapisz się już dzisiaj!</button>
                        </div>
                    </div>
                </section>
            </main>
        );
    }

    renderTimeline() {
        $(function () {
            window.sr = ScrollReveal();
            let sr = window.sr;

            if ($(window).width() < 768) {

                if ($(".timeline-content").hasClass("js--fadeInLeft")) {
                    $(".timeline-content").removeClass("js--fadeInLeft").addClass("js--fadeInRight");
                }

                sr.reveal(".js--fadeInRight", {
                    origin: "right",
                    distance: "0px",
                    easing: "ease-in-out",
                    duration: 800,
                });

            } else {

                sr.reveal(".js--fadeInLeft", {
                    origin: "left",
                    distance: "0px",
                    easing: "ease-in-out",
                    duration: 800,
                });

                sr.reveal(".js--fadeInRight", {
                    origin: "right",
                    distance: "0px",
                    easing: "ease-in-out",
                    duration: 800,
                });

            }

            sr.reveal(".js--fadeInLeft", {
                origin: "left",
                distance: "0px",
                easing: "ease-in-out",
                duration: 800,
            });

            sr.reveal(".js--fadeInRight", {
                origin: "right",
                distance: "0px",
                easing: "ease-in-out",
                duration: 800,
            });
        });
    }
}


export default MainPage;
