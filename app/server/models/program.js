module.exports = (sequelize, DataTypes) => {
    const Program = sequelize.define("Program", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        start: DataTypes.TIME,
        end: DataTypes.TIME,
        date: DataTypes.DATE,
        isPayed: DataTypes.BOOLEAN
    });
        Program.associate = (models) => {
            Program.belongsTo(models.Subject, {
                foreignKey: "subjectId",
                as: "subject"
            });
            Program.belongsTo(models.User, {
                foreignKey: "userId",
                as: "user"
            });
        };
    return Program;

};