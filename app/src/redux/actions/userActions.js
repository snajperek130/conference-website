import {post} from "./customActions";
import {UserAPI} from "../../../config/api";

export function login(data) {
    return ((dispatch) => {
        post(dispatch, data, UserAPI.login);
    });
}

export function updateProfile(data) {
    return ((dispatch) => {
        post(dispatch, data, UserAPI.update);
    });
}


export function getProfile(data) {
    return ((dispatch) => {
        post(dispatch, data, UserAPI.getUser);
    });
}

export function deleteUser(data) {
    return ((dispatch) => {
        post(dispatch, data, UserAPI.delete);
    });
}

export function getUsers(data) {
    return ((dispatch) => {
        post(dispatch, data, UserAPI.getUsers);
    });
}

