import {UserActions} from "../../../config/actionNames";

export function UserReducer(state = {}, action) {
    switch (action.type) {
        case UserActions.REGISTER_SUCCESFULL, UserActions.LOGIN_SUCCESFULL, UserActions.GET_USER:
            return action.payload.data;
    }
    return state;
}

