import {AttachmentAPI} from "../../config/api";
import * as _ from "lodash";

const multer = require("multer");
const path = "./server/files/";

module.exports = (app) => {
    const upload = multer({dest: path});
    app.post(AttachmentAPI.upload, upload.single("file"), ((req, res, next) => {
        let data = req.files.file;
        if (!_.isArray(data)) {
            data = [data];
        }
        const arrayPromises = [];
        _.forEach((data), file => {
            arrayPromises.push(new Promise((resolve, reject) => {
                file.mv(path + file.name, ((err) => {
                    if (err) {
                        reject("ERRRR :/");
                    } else {
                        resolve("SUCCESS");
                    }
                }));
            }));
        });
        Promise.all(arrayPromises)
            .then((res) => {
                console.log(res);
            }).catch((err) => {
            console.log(err);
        });
    }));

    app.post(AttachmentAPI.getFiles, ((req, res) => {

    }));
};


