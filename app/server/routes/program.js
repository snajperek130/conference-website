import * as programController from "../controllers/program";
import {ProgramAPI} from "../../config/api";

module.exports = (app) => {
    app.get(ProgramAPI.getWholeProgram, (req, res) => {
        programController.getWholeProgram(req, res);
    });

    app.post(ProgramAPI.createProgram, (req, res) => {
        programController.create(req, res);
    });
};


