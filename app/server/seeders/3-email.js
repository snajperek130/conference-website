module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert("Emails", [
			{
				id: 1,
				type: "paymentReminder",
			},
			{
				id: 2,
				type: "confirmation",
			},
			{
				id: 3,
				type: "timeExpired",
			}
		], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete("Emails", null, {});
	}
};
