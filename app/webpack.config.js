var path = require("path");

const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
	entry: ["./src/index.js"],
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "public")
	},
	watch: true,
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: "babel-loader",
				query: {
					presets: ["react", "es2015", "stage-1"]
				}
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: "css-loader"
				})
			},
			{
				test: [/\.png$/, /\.jpeg$/, /\.jpg$/],
				exclude: /node_modules/,
				loader: "file-loader"

			},
			{
				test: /\.(otf|eot|svg|ttf|woff|woff2)$/,
				loader: "file-loader"
			}
		]
	}, plugins: [
		new ExtractTextPlugin({
			filename: "style.css",
			allChunks: true
		})
	]
};