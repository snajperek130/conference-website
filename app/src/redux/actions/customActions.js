import axios from "axios/index";
import * as config from "../../../config/baseConfigFile";
import {ErrorCustomType} from "../../../config/actionNames";

export function post(dispatch, data, url) {
    axios.post(config.url + url, data)
        .then((response) => {
            dispatch({type: response.data.action, payload: response.data});
        })
        .catch((err) => {
            dispatch({type: ErrorCustomType.ERROR_HANDLING, payload: err.response});
        });
}

export function get(dispatch, url) {
    axios.get(config.url + url)
        .then((response) => {
            dispatch({type: response.data.action.type, payload: response.data});
        })
        .catch((err) => {
            dispatch({type: ErrorCustomType.ERROR_HANDLING, payload: err.response});
        });
}