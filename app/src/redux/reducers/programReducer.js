import {ProgramActions} from "../../../config/actionNames";

export function ProgramReducer(state = [], action) {
    switch (action.type.type) {
        case ProgramActions.PROGRAM_FETCHED.type: {
            return {...state, program: [...action.payload.data]};
        }
    }

    return state;
}