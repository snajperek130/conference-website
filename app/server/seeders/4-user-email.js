module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert("UserEmails", [
			{
				id: 1,
				userId: 1,
				emailId: 1
			},
			{
				id: 2,
				userId: 2,
				emailId: 1
			},
			{
				id: 3,
				userId: 3,
				emailId: 2
			}
		], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete("UserEmails", null, {});
	}
};
