import React from "react";
import {render} from "react-dom";
import {BrowserRouter} from "react-router-dom";
import App from "./client";
import {applyMiddleware, createStore} from "redux";
import {logger} from "redux-logger";
import Provider from "react-redux/es/components/Provider";
import reducers from "./redux/reducers/index";
import thunk from "redux-thunk";

const middleware = applyMiddleware(thunk, logger);
const store = createStore(reducers, middleware);

render((
    <Provider store={store}>
        <BrowserRouter>
            <App>
            </App>
        </BrowserRouter>
    </Provider>
), document.getElementById("app"));
