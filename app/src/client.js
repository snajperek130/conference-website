import React, {Component} from "react";
import Main from "./main";
import Menu from "./mainModule/components/navigationBar";
import Footer from "./mainModule/components/footer";
import ReduxToastr from "react-redux-toastr";

class App extends Component {
	render() {
		return (
			<div>
                <ReduxToastr
                    timeOut={4000}
                    newestOnTop={false}
                    preventDuplicates
                    position="top-left"
                    transitionIn="fadeIn"
                    transitionOut="fadeOut"
                    progressBar/>
				<Menu/>
				<Main/>
				<Footer/>
			</div>
		);
	}
}

export default App;
