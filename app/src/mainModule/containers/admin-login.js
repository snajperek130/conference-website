import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {login} from "../../redux/actions/userActions";
import {bindActionCreators} from "redux";

class AdminLogin extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin(e) {
        e.preventDefault();
        const data = {
            email: this.emailLogin.value,
            password: this.passwordLogin.value
        };
        this.props.login(data);
    }

    render() {
        return (
            <main className="down">
                <header className="header-section">
                    <div className="header-title">Login</div>
                </header>
                <section className="register-login-section">
                    <div className="top-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">

                                    <div className="form-box">
                                        <div className="form-top">
                                            <div className="form-top-left">
                                                <h3>Zaloguj się</h3>
                                                <p>Wpisz nazwę użytkownika i hasło</p>
                                            </div>
                                            <div className="form-top-right">
                                                <i className="fa fa-lock"/>
                                            </div>
                                        </div>
                                        <div className="form-bottom">
                                            <form onSubmit={this.handleLogin} role="form" action="" method="post"
                                                  className="login-form">
                                                <div className="form-group">
                                                    <label className="sr-only"
                                                           form="form-username">Nazwa użytkownika</label>
                                                    <input ref={(input) => {
                                                        this.emailLogin = input;
                                                    }} type="text"
                                                           name="email"
                                                           placeholder="UNazwa użytkownika..."
                                                           className="form-username form-control"
                                                           id="form-username"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="sr-only"
                                                           form="form-password">Hasło</label>
                                                    <input ref={(input) => {
                                                        this.passwordLogin = input;
                                                    }} type="password"
                                                           name="password"
                                                           placeholder="Hasło..."
                                                           className="form-password form-control"
                                                           id="form-password"
                                                    />
                                                </div>
                                                <button type="submit" className="btn">Zaloguj się
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </main>
        );
    }
}

AdminLogin.propTypes = {login: PropTypes.func};

function mapStateToProps(state) {
    return {
        program: state.program.program
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login: login
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminLogin);
