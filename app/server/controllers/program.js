import {ProgramActions, SubjectActions, UserActions} from "../../config/actionNames";

const Program = require("../models").Program;
const User = require("../models").User;
const Subject = require("../models").Subject;

module.exports = {
    getWholeProgram(req, res) {
        return Program.findAll({
            include: [
                {
                    model: User,
                    as: "user"
                },
                {
                    model: Subject,
                    as: "subject"
                }
            ],
        })
            .then(program => console.log(program))
            .catch(err =>  console.log(err));
    },
    getProgram(req, res) {
        return Program.findAll({
            where: [{loanId: req.body.id}],
            include: [
                {
                    model: User,
                    as: "user"
                },
                {
                    model: Subject,
                    as: "subject"
                }
            ],
        })
            .then(program => res.status(200).send({data: program, action: ProgramActions.PROGRAM_FETCHED}))
            .catch(err => res.status(400).send(ProgramActions.PROGRAM_REJECTED));
    },
    async create(req, res) {
        try {
            const subject = await Subject.create(req.body.subject);
            if (!subject) {
                return res.status(400).send(SubjectActions.UNKNOW_SUBJECT);
            }
            console.log("subj");
            const user = await User.create(req.body.user);
            if (!user) {
                return res.status(400).send(UserActions.UNKNOWN_USER);
            }
            console.log("user");
            return Program
                .create({
                    subjectId: subject.dataValues.id,
                    userId: user.dataValues.id,
                    start: null,
                    end: null,
                    date: req.body.program.date,
                    isPayed: false
                })
                .then(program => res.status(200).send({data: program, action: ProgramActions.PROGRAM_CREATED}))
                .catch(err => res.status(400).send(ProgramActions.PROGRAM_REJECTED));

        } catch (err) {
            throw new Error(err);
        }
    },
};