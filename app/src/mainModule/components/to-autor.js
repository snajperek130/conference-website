import React from "react";

const ToAutor = () => {
    return (
        <main className="down">
            <header className="header-section">
                <div className="header-title">Dla autora</div>
            </header>
            <title>Konferencja jest poświęcona następującej tematyce:</title>
            <section className="subjects-section">
                <div className="container center">
                    <div className="row">
                        <div className="col-md-6 col-sm-6 col-xs-12">
                            <img alt="about" className="img-responsive circle image"
                                 src={require("../assets/photos/informatic.jpeg")}/>
                        </div>
                        <div className="col-md-6 col-sm-6 col-xs-12 text">
                            <div>Informatyka</div>
                            <div className="description">
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
                            <img alt="about" className="img-responsive circle image"
                                 src={require("../assets/photos/electric.jpeg")}/>
                        </div>
                        <div className="col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6 text">
                            <div>Elektrotechnika</div>
                            <div className="description">
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-sm-6 col-xs-12">
                            <img alt="about" className="img-responsive circle image"
                                 src={require("../assets/photos/informatic.jpeg")}/>
                        </div>
                        <div className="col-md-6 col-sm-6 col-xs-12 text">
                            <div>Informatyka</div>
                            <div className="description">
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
                            <img alt="about" className="img-responsive circle image"
                                 src={require("../assets/photos/electric.jpeg")}/>
                        </div>
                        <div className="col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6 text">
                            <div>Elektrotechnika</div>
                            <div className="description">
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                                opisop isopisopisopisopiso pisopisopiso pisopisopisopiso pisopisopisopis opisopis
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
};

export default ToAutor;
