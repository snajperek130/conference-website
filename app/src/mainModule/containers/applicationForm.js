import React from "react";
import {uploadFile} from "../../redux/actions/attachemntActions";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import Dropzone from "react-dropzone";
import {bindActionCreators} from "redux";
import * as _ from "lodash";
import {createProgram} from "../../redux/actions/programActions";
import moment from "moment";
import {toastr} from "react-redux-toastr";

class ApplicationForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            translatedDays: ["Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"],
            days: [new Date(2018, 2, 20), new Date(2018, 2, 21), new Date(2018, 2, 22), new Date(2018, 2, 23)],
            accepted: [],
            rejected: [],
            type: ""
        };
        this.save = this.save.bind(this);
        this.removeFile = this.removeFile.bind(this);
    }

    dropAction({accepted, rejected}) {
        let files = this.state.accepted;
        _.forEach(accepted, file => {
            files.push(file);
        });
        this.setState({
            accepted: files,
            rejected: rejected
        });
    }

    removeFile(name) {
        const index = _.findIndex(this.state.accepted, acceptedFile => {
            return acceptedFile.name === name;
        });
        let files = this.state.accepted;
        files.splice(index, 1);
        this.setState({
            accepted: files
        });
    }

    sendFiles() {
        let files = new FormData();
        _.forEach(this.state.accepted, acceptedFile => {
            files.append("file", acceptedFile, acceptedFile.name);
        });
        // if (files.length > 0) {
            this.props.uploadFile(files);
        // }
    }

    createProgram() {
        const user = {
            name: this.name.value,
            degree: this.degree.value,
            institution: this.institution.value,
            address: this.address.value,
            email: this.email.value,
            additionalLecturers: this.additionalLecturers.value
        };
        const subject = {
            title: this.title.value,
            description: this.description.value,
            type: this.state.type
        };

        const parts = this.date.value.split("/");

        const program = {
            date: new Date(parseInt(parts[2]), parseInt(parts[1] - 1), parseInt(parts[0]))
        };
        const data = {user: user, subject: subject, program: program};
        this.props.createProgram(data);
    }

    save(e) {
        e.preventDefault();
        this.createProgram();
        this.sendFiles();
        toastr.success("The title", "The message");
    }

    updateRadio(value) {
        this.setState({
            translatedDays: this.state.translatedDays,
            days: this.state.days,
            accepted: this.state.accepted,
            type: value
        });
    }

    render() {
        return (
            <main className="down">
                <header className="header-section">
                    <div className="header-title">Uczestnicy</div>
                </header>
                <section className="application-section">
                    <div className="container">
                        <form>
                            <div className="form-group">
                                <label>Email</label>
                                <input ref={(input) => {
                                    this.email = input;
                                }} type="email" className="form-control"
                                       placeholder="Wpisz swój email"/>
                                <small className="form-text text-muted">Nie udostępnimy twojego emaila zewnętrznym
                                    źródłom.
                                </small>
                            </div>
                            <div className="form-group">
                                <label>Imie i nazwisko</label>
                                <input ref={(input) => {
                                    this.name = input;
                                }} type="text" className="form-control"
                                       aria-describedby="emailHelp" placeholder="Imie i nazwisko"/>
                            </div>
                            <div className="form-group">
                                <label>Adres</label>
                                <input ref={(input) => {
                                    this.address = input;
                                }} type="text" className="form-control"
                                       aria-describedby="emailHelp" placeholder="Wpisz swój adres"/>
                            </div>
                            <div className="form-group">
                                <label>Stopień naukowy</label>
                                <input ref={(input) => {
                                    this.degree = input;
                                }} type="text" className="form-control"
                                       placeholder="Wpisz stopień naukowy, np. mgr"/>
                            </div>
                            <div className="form-group">
                                <label>Instytucja naukowu</label>
                                <input ref={(input) => {
                                    this.institution = input;
                                }} type="text" className="form-control"
                                       placeholder="Wpisz Instytucja naukową, np. Uniwersytet Rzeszowski"/>
                            </div>
                            <div className="form-group">
                                <label>Jeśli ten wykład prowadzi więcej niż jedna osoba, wpisz imię i nazwisko
                                    pozostałych:</label>
                                <input ref={(input) => {
                                    this.additionalLecturers = input;
                                }} className="form-control" placeholder="Wpisz imię i nazwisko"/>
                            </div>
                            <div className="form-group">
                                <label>Wybierz dzień prezentacji:</label>
                                <select ref={(input) => {
                                    this.date = input;
                                }} className="form-control">
                                    {
                                        this.state.days.map(day =>
                                            <option
                                                key={day.getDay()}>{moment(day).format("DD/MM/YYYY")}</option>
                                        )
                                    }
                                </select>
                            </div>

                            <div className="form-group">
                                <label>Podaj tytuł prezentacji:</label>
                                <input ref={(input) => {
                                    this.title = input;
                                }} maxLength="100" className="form-control" placeholder="Wpisz tytuł"/>
                            </div>

                            <div className="form-group">
                                <label>Wybierz rodzaj prezentacji:</label>
                                <div className="radio">
                                    <label><input checked={this.state.type === "odczyt"}
                                                  onClick={() => this.updateRadio("odczyt")} type="radio"
                                                  name="odczyt"/>odczyt</label>
                                </div>
                                <div className="radio">
                                    <label><input checked={this.state.type === "plakat"}
                                                  onClick={() => this.updateRadio("plakat")} type="radio"
                                                  name="plakat"/>plakat</label>
                                </div>
                            </div>

                            <div className="form-group">
                                <label>Opisz swoją prezentację:</label>
                                <textarea ref={(input) => {
                                    this.description = input;
                                }} maxLength="400" className="form-control" rows="3"/>
                            </div>

                            <div className="form-group">
                                <div className="dropzone">
                                    <Dropzone accept=".doc, .pdf"
                                              onDrop={(accepted, rejected) => {
                                                  this.dropAction({accepted, rejected});
                                              }}>
                                        <p>Przeciągnij pliki na to pole, lub kliknij i wybierz.</p>
                                        <p>Tylko pliki z rozszerzeniem .doc lub .pdf</p>
                                    </Dropzone>
                                </div>
                                <aside>
                                    <h2>Pliki do zapisu</h2>
                                    <ul>
                                        {
                                            this.state.accepted.map(f =>
                                                <li key={f.name}>{f.name} - {f.size} bytes
                                                    <span>&nbsp;</span> <i onClick={() => this.removeFile(f.name)}
                                                                           key={f.name} ref={(input) => {
                                                        this.fileToDelete = input;
                                                    }}
                                                                           className="fa fa-times" aria-hidden="true"/>
                                                </li>)
                                        }
                                    </ul>
                                    <h2>Pliki, które nie będą zapisane</h2>
                                    <ul>
                                        {
                                            this.state.rejected.map(f =>
                                                <li key={f.name}>{f.name} - {f.size} bytes
                                                </li>)
                                        }
                                    </ul>
                                </aside>
                            </div>
                            <button onClick={this.save} type="submit" className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </section>
            </main>
        );
    }
}

function mapStateToProps(state) {
    return {
        errorMessage: state.error
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        uploadFile: uploadFile,
        createProgram: createProgram
}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationForm);

ApplicationForm.propTypes = {uploadFile: PropTypes.func};
ApplicationForm.propTypes = {createProgram: PropTypes.func};

