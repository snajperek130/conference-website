import React from "react";

class Members extends React.Component {

    render() {
        return (
            <main className="down">
                <header className="header-section">
                    <div className="header-title">Uczestnicy</div>
                </header>
                <section className="members-table-section">
                    <div className="container">
                        <table className="table">
                            <thead className="table-header">
                            <tr>
                                <th>Szkoła Główna Gospodarstwa Wiejskiego w Warszawie</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>dr hab. Adam Ciarkowski</td>
                            </tr>
                            <tr>
                                <td>dr hab. inż Imed EI Fray</td>
                            </tr>
                            <tr>
                                <td>dr hab. inż. Michał Kruk</td>
                            </tr>
                            </tbody>
                        </table>

                        <table className="table">
                            <thead className="table-header">
                            <tr>
                                <th>Politechnika Warszawska</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>dr hab. Joanna Landmesser</td>
                            </tr>
                            <tr>
                                <td>dr hab. inż. Arkadiusz Orłowski, prof. SGGQ</td>
                            </tr>
                            <tr>
                                <td>dr hab. Marian Rusek</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </section>
            </main>
        );
    }
}

export default Members;
