import {ErrorCustomType} from "../../../config/actionNames";

/**
 * @return {string}
 */
export function ErrorHandlerReducer(state = "", action) {
    if (action.payload) {
        switch (action.type) {
            case ErrorCustomType.ERROR_HANDLING:
                console.log("err");
                return {
                    ...state, error: action.payload.data
                };
        }
    }
    return state;
}