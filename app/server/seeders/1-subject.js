module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert("Subjects", [
			{
				id: 1,
				title: "Herbert",
				description: "322-123-21-12",
                type: "read"
			},
			{
				id: 2,
				title: "gdsg",
				description: "asdgsags",
                type: "poster"
			},
			{
				id: 3,
				title: "dsgsd",
				description: "asdfdsaf",
				type: "read"
			}
		], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete("Subjects", null, {});
	}
};