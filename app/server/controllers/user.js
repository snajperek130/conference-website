import {EmailActions, ProgramActions, UserActions} from "../../config/actionNames";
import * as randomstring from "randomstring";
import * as mailerMaker from "../emailSending/emailMaker";
import * as bcrypt from "bcryptjs";
import * as MailerConfig from "../config/mailerConfig";

const User = require("../models").User;
const UserSubject = require("../models").UserSubject;
const UserEmail = require("../models").UserEmail;

module.exports = {
    async register(req, res) {
        try {
            const user = await this.getUserByEmail(req.body.email);
            if (user) {
                res.status(409).send(UserActions.USER_ALREADY_EXISTS);
            }
            req.body.password = await hashPassword(req.body.password);
            req.body.secretToken = randomstring.generate();
            req.body.active = false;
            const createdUser = await this.create(req, res);
            mailerMaker.sendEmailTemlate([
                {
                    email: MailerConfig.TEST_USER_EMAIL,
                    user: createdUser
                }], "activate")
                .then((email) => {
                    res.status(200).send({data: email, action: EmailActions.REGISTER_EMAIL_SENDED});
                })
                .catch(() => {
                    res.status(400).send(EmailActions.REGISTER_EMAIL_REJECTED);
                });
        } catch (err) {
            throw new Error(err);
        }
    },
    async login(req, res) {
        const user = await this.getUserByEmail(req.body.email);
        if (!user) {
            return res.status(400).send(UserActions.UNKNOWN_USER);
        } else {
            const isValid = await comparePassword(req.body.password, user.password);

            if (!isValid) {
                return res.status(400).send(UserActions.UNKNOWN_PASSWORD);
            }

            if (isValid && !user.active) {
                return res.status(403).send(UserActions.NOT_VERIFIED);
            }

            if (isValid) {
                res.status(200).send({data: user, type: UserActions.LOGIN_SUCCESFULL});
            }
        }
    },
    async verify(req, res) {
        try {
            const {secretToken} = req.body;
            const user = await this.getUserByToken(secretToken);
            if (!user) {
                return res.status(400).send(UserActions.UNKNOWN_USER);
            }
            user.active = true;
            user.secretToken = "";
            this.updateUser(user)
                .then(user => res.status(200).send(user))
                .catch(err => res.status(400).send(UserActions.UPDATE_ERROR));
        } catch (err) {
            throw new Error(err);
        }
    },
    getAllUsers(req, res) {
        return User.all()
            .then(user => res.status(200).send(user))
            .catch(error => res.status(400).send(error));
    },
    getUserById(req, res) {
        req.params.id = 1;
        return User.findOne({
            where: {
                id: req.params.id
            }
        });
    },
    getUserByToken(secretToken) {
        return User.findOne({
            where: {
                secretToken: secretToken
            }
        });
    },
    getUserByEmail(email) {
        return User.findOne({
            where: {
                email: email
            }
        });
    },
    async create(req, res) {
        return User
            .create({
                name: req.name,
                degree: req.degree,
                institution: req.institution,
                address: req.address,
                email: req.email,
                additionalLecturers: req.additionalLecturers
            });
    },
    destroy(req, res) {
        return User
            .findById(req.params.id)
            .then(user => {
                if (!user) {
                    return res.status(400).send({
                        message: "User Not Found",
                    });
                }
                UserSubject.findAll({
                    where: {
                        userId: req.params.id
                    }
                }).then(userSubject => {
                    userSubject.destroy();
                });
                UserEmail.findAll({
                    where: {
                        userId: req.params.id
                    }
                }).then(userEmail => {
                    userEmail.destroy();
                });

                return user
                    .destroy()
                    .then(() => res.status(200).send())
                    .catch(error => res.status(400).send(error));
            });
    },
    updateUser(req, res) {
        return User
            .findById(1)
            .then(user => {
                if (!user) {
                    return res.status(404).send(UserActions.UNKNOWN_USER);
                }
                return user
                    .update({
                        firstname: req.body.firstname,
                        lastname: req.body.lastName,
                        dateOfBirth: req.body.dateOfBirth,
                        university: req.body.university,
                        degreeTitle: req.body.degreeTitle
                    })
                    .then(user => res.status(200).send({data: user, type: UserActions.USER_UPDATED}))
                    .catch(error => res.status(400).send(UserActions.UPDATE_ERROR));
            })
            .catch(error => res.status(400).send(UserActions.UNKNOWN_USER));
    },
    async updatePassword(req, res) {
        try {
            const newPassword = await hashPassword(req.body.password);
            return User
                .findById(1)
                .then(user => {
                    if (!user.dataValues) {
                        return res.status(404).send(UserActions.UNKNOWN_USER);
                    }

                    return user
                        .update({
                            password: newPassword
                        })
                        .then(user => res.status(200).send({data: user, type: UserActions.USER_UPDATED}))
                        .catch(error => res.status(400).send(UserActions.USER_PASSWORD_UPDATE_ERROR));
                })
                .catch(error => res.status(400).send(UserActions.UNKNOWN_USER));
        } catch(err) {
            throw new Error(err);
        }
    }
};

async function hashPassword(password) {
    try {
        const salt = bcrypt.genSaltSync(10);
        return await bcrypt.hash(password, salt);
    } catch (err) {
        throw new Error("Hashing failed", err);
    }
}

async function comparePassword(inputPassword, hashedPassword) {
    try {
        return await bcrypt.compare(inputPassword, hashedPassword);
    } catch (err) {
        throw new Error(err);
    }
}