module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert("Users", [
            {
                id: 1,
                name: "Herbert",
                degree: "Prof",
                institution: "Rzeszow",
                address: "Gdzieś tam",
                email: "@",
                additionalLecturers: "Konrad Heh"
            },
            {
                id: 2,
                name: "gfdgfd",
                degree: "Prof dr",
                institution: "Rzeszow",
                address: "Gdzieś tsdaf sam",
                email: "@@",
                additionalLecturers: "Konfsarad Heh"
            },
            {
                id: 3,
                name: "Ktoś",
                degree: "Prof dr hab",
                institution: "Rzeszow",
                address: "csa af a tam",
                email: "@@@",
                additionalLecturers: "Konrad csa"
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete("Users", null, {});
    }
};
