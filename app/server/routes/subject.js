import * as subjectController from "../controllers/subject";

module.exports = (app) => {
	app.get("/api/subjects", (req, res) => {
		subjectController.getAllSubjects(req, res);
	});
	app.post("/api/update-subject", (req, res) => {
		subjectController.update(req, res);
	});
};


