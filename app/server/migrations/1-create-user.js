module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable("Users", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
            name: {
				type: Sequelize.STRING
			},
            degree: {
                type: Sequelize.STRING
			},
            institution: {
				type: Sequelize.STRING
			},
            address: {
				type: Sequelize.STRING
			},
            email: {
				type: Sequelize.STRING
			},
            additionalLecturers: {
				type: Sequelize.STRING
			},
			createdAt: {
				type: Sequelize.DATE
			},
			updatedAt: {
				type: Sequelize.DATE
			}
		});
	},
	down: (queryInterface, Sequelize) => queryInterface.dropTable("Users"),
};