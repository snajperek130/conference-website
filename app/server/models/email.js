module.exports = (sequelize, DataTypes) => {
    const Email = sequelize.define("Email", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        type: DataTypes.STRING
    });
    Email.associate = (models) => {
        Email.belongsToMany(models.User, {
            through: "UserEmail",
            foreignKey: "userId",
            keyThrough: "emailId"
        });
    };
    return Email;
};
