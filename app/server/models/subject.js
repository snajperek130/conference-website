module.exports = (sequelize, DataTypes) => {
	const Subject = sequelize.define("Subject", {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		title: DataTypes.STRING,
		description: DataTypes.STRING,
		type: DataTypes.STRING
	});
    return Subject;
};
