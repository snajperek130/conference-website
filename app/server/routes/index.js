module.exports = (app) => {
	require("./user")(app);
	require("./email")(app);
	require("./subject")(app);
    require("./program")(app);
    require("./attachment")(app);
    require("./admin")(app);
};
