module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable("Subjects", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			title: {
				type: Sequelize.STRING
			},
			description: {
				type: Sequelize.STRING
			},
			type: {
				type: Sequelize.STRING
			},
			createdAt: {
				type: Sequelize.DATE
			},
			updatedAt: {
				type: Sequelize.DATE
			}
		});
	},
	down: (queryInterface, Sequelize) => queryInterface.dropTable("Subjects"),
};