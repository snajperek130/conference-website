import React from "react";
import {Navbar, NavItem, Nav} from "react-bootstrap";

const NavigationBar = () => (
    <Navbar className="header" inverse collapseOnSelect fixedTop>
        <Navbar.Header>
            <Navbar.Brand>
                <a href="/"><img href="/informations" id="logo" alt="about" className="img-responsive image"
                                 src={require("../assets/photos/logo.jpg")}/></a>

            </Navbar.Brand>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav navbar pullRight>
                <NavItem eventKey={1} href="/informations">Informacje</NavItem>
                <NavItem eventKey={2} href="/program">Program</NavItem>
                <NavItem eventKey={3} href="/members">Uczestnicy</NavItem>
                <NavItem eventKey={4} href="/committee ">Komitety</NavItem>
                <NavItem eventKey={5} href="/to-autor">Dla autora</NavItem>
                <NavItem eventKey={6} href="/payments">Opłaty</NavItem>
                <NavItem eventKey={7} href="/contact">Kontakt</NavItem>
                <NavItem eventKey={8} href="/register-login">Rejestracja/Logowanie</NavItem>
                <NavItem eventKey={9} href="/account">Account</NavItem>
                <NavItem eventKey={10} href="/admin">Admin</NavItem>
                <NavItem eventKey={10} href="/form">Ap form</NavItem>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default NavigationBar;