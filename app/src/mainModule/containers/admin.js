import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {getFiles, removeFile} from "../../redux/actions/attachemntActions";
import {deleteUser, getProfile, getUsers} from "../../redux/actions/userActions";
import {bindActionCreators} from "redux";

class AdminPanel extends React.Component {

    constructor() {
        super();
        this.state = {};
    }

    componentDidMount() {
        // console.log(this.props.getUsers());
    }

    render() {
        return (
            <main className="down">

            </main>
        );
    }
}

function mapStateToProps(state) {
    return {
        errorMessage: state.error,
        users: state.user,
        attachments: state.attachment
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getUses: getUsers,
        deleteUse: deleteUser,
        getProfile: getProfile,
        getFiles: getFiles,
        deleteFile: removeFile,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel);

AdminPanel.propTypes = {getProfile: PropTypes.func};
AdminPanel.propTypes = {updateProfile: PropTypes.func};
AdminPanel.propTypes = {updatePassword: PropTypes.func};
AdminPanel.propTypes = {uploadFile: PropTypes.func};
AdminPanel.propTypes = {removeFile: PropTypes.func};
AdminPanel.propTypes = {getFiles: PropTypes.func};
AdminPanel.propTypes = {deleteFile: PropTypes.func};
AdminPanel.propTypes = {errorMessage: PropTypes.string};