import {combineReducers} from "redux";
import {ProgramReducer} from "./programReducer";
import {UserReducer} from "./userReducer";
import {ErrorHandlerReducer} from "./errorHandlerReducer";
import {reducer as toastrReducer} from "react-redux-toastr";


export default combineReducers({
    program: ProgramReducer,
    user: UserReducer,
    error: ErrorHandlerReducer,
    toastr: toastrReducer
});