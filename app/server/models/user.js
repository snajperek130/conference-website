module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        name: DataTypes.STRING,
        degree: DataTypes.STRING,
        institution: DataTypes.STRING,
        address: DataTypes.STRING,
        email: DataTypes.STRING,
        additionalLecturers: DataTypes.STRING
    });
    return User;
};
