import {get, post} from "./customActions";
import {ProgramAPI} from "../../../config/api";

export function getWholeProgram() {
    return ((dispatch) => {
        get(dispatch, ProgramAPI.getWholeProgram);
    });
}

export function createProgram(data) {
    return ((dispatch) => {
        post(dispatch, data, ProgramAPI.createProgram);
    });
}