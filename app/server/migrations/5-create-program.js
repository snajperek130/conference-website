module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("Programs", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            subjectId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: "Subjects",
                    key: "id",
                    as: "subject",
                    through: "subject"
                }
            },
            userId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: "Users",
                    key: "id"
                }
            },
            date: {
                type: Sequelize.DATE
            },
            start: {
                type: Sequelize.TIME
            },
            end: {
                type: Sequelize.TIME
            },
            isPayed: {
                type: Sequelize.BOOLEAN
            },
            createdAt: {
                type: Sequelize.DATE
            },
            updatedAt: {
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => queryInterface.dropTable("Programs"),
};

