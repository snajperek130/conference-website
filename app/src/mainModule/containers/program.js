import React from "react";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getWholeProgram} from "../../redux/actions/programActions";
import ErrorDisplay from "../components/errorHandling";
import * as moment from "moment";

class Program extends React.Component {

    constructor() {
        super();
        this.state = {
            days: ["Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"]
        };
    }

    componentDidMount() {
        console.log(this.props.getWholeProgram());
    }


    getProgram() {
        return (
            <table className="table">
                <thead className="table-header">
                <tr>
                    <th/>
                    <th className="center-header">Piątek</th>
                    <th/>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th>Godzina</th>
                    <th>Temat</th>
                    <th>Prowadzący</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                </tbody>
            </table>
        );
        // })
    }

    render() {
        return (
            <main className="down">
                <header className="header-section">
                    <div className="header-title">Program</div>
                </header>
                {(() => {
                    if (this.props.errorMessage.error) {
                        return (<ErrorDisplay message={this.props.errorMessage.error}/>);
                    }
                })()}
                <section className="files-section">
                    <div className="container">
                        <div className="row">
                            <ul>
                                <li>
                                    <i className="glyphicon glyphicon-list-alt program-icon"/>
                                    <span className="program-icon-text">Plan konferencji</span>
                                </li>
                                <li>
                                    <i className="	glyphicon glyphicon-blackboard program-icon"/>
                                    <span className="program-icon-text">Szablon artykułu</span>
                                </li>
                                <li>
                                    <i className="glyphicon glyphicon-folder-close program-icon"/>
                                    <span className="program-icon-text">Folder konferencji</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section className="program-table-section">
                    <div className="container">
                        {(() => {
                            this.getProgram();
                        })()}
                    </div>
                </section>
            </main>
        );
    }
}

Program.propTypes = {getWholeProgram: PropTypes.func};
Program.propTypes = {program: PropTypes.array};
Program.propTypes = {errorMessage: PropTypes.string};

function mapStateToProps(state) {
    return {
        program: state.program,
        errorMessage: state.error
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getWholeProgram: getWholeProgram
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Program);
