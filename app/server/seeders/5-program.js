module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert("Programs", [
            {
                id: 1,
                userId: 1,
                subjectId: 1,
                date: new Date("01-01-2018"),
                start: new Date().getTime(),
                end: new Date().getTime()+1,
                isPayed: true
            },
            {
                id: 2,
                userId: 2,
                subjectId: 2,
                date: new Date("01-01-2018"),
                start: new Date().getTime(),
                end: new Date().getTime()+1,
                isPayed: true
            },
            {
                id: 3,
                userId: 3,
                subjectId: 3,
                date: new Date("01-01-2018"),
                start: new Date().getTime(),
                end: new Date().getTime()+1,
                isPayed: false
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete("Programs", null, {});
    }
};




