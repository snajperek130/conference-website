import {AttachemntActions} from "../../../config/actionNames";
import {post} from "./customActions";
import {AttachmentAPI} from "../../../config/api";

export function uploadFile(data) {
    return ((dispatch) => {
        post(dispatch, data, AttachmentAPI.upload);
    });
}

export function getFiles(data) {
    return ((dispatch) => {
        post(dispatch, data, AttachmentAPI.getFiles);
    });
}

export function removeFile(data) {
    return ((dispatch) => {
        post(dispatch, data, AttachmentAPI.delete);
    });
}
