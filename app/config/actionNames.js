export const ProgramActions = {
    PROGRAM_FETCHED: {type: "GET_PROGRAM", message: "Program pobrany"},
    PROGRAM_REJECTED: "Problem z pobraniem programu",
    PROGRAM_CREATED: {type: "PROGRAM_CREATED", message: "Program został stworzony"}
};

export const UserActions = {
    REGISTER_SUCCESFULL: {type: "REGISTER_SUCCESFULL", message: "Rejestracja przebiegła prawidłowo"},
    USER_ALREADY_EXISTS: {type: "USER EXISTS", message: "Użytkownik o podanym emailu już istnieje!"},
    LOGIN_SUCCESFULL: {type: "LOGIN SUCCESFULL", message: "Logowanie przebiegło prawidłowo!"},
    UNKNOWN_PASSWORD: "Nieznane hasło, spróbuj ponownie",
    UNKNOWN_USER: "Nieznany użytkownik",
    NOT_VERIFIED: "Proszę o potwierdzenie użytkownika poprzez email",
    USER_UPDATED: "Użytkownik został zaaktualizowany",
    UPDATE_ERROR: "Problem z aktualizacją użytkownika",
    USER_PASSWORD_UPDATED: "Hasło zostało zaaktualizowane",
    USER_PASSWORD_UPDATE_ERROR: "Hasło nie zostało zaaktualizowane",
    GET_USER: "Użytkownik istnieje"
};

export const SubjectActions = {
    UNKNOW_SUBJECT: "Nieznany temat"
};

export const EmailActions = {
    REGISTER_EMAIL_SENDED: {type: "REGISTER EMAIL HAS BEEN SENT", message: "Email do rejestracji został wysłany"},
    REGISTER_EMAIL_REJECTED: "Nastąpił problem z wysłaniem emaila"
};

export const FileActions = {
    FILE_SAVED: {type: "FILE SAVED", message: "Plik został pomyślnie zapisany"},
    FILE_REJECTED: "Plik nie został pomyślnie zapisany"
};

export const ErrorCustomType = {
    ERROR_HANDLING: "ERROR HANDLE"
};
