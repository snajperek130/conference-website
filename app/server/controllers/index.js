const user = require("./user");
const subject = require("./subject");
const program = require("./program");
const admin = require("./admin");

module.exports = {
	user,subject,program, admin
};
