module.exports = (sequelize, DataTypes) => {
	const UserEmail = sequelize.define("UserEmail", {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		}
	});
	UserEmail.associate = (models) => {
		UserEmail.belongsToMany(models.Email, {
			foreignKey: "emailId",
			through: "UserEmail"
		});
	};
	UserEmail.associate = (models) => {
		UserEmail.belongsToMany(models.User, {
			foreignKey: "userId",
			through: "UserEmail"
		});
	};
	return UserEmail;
};
