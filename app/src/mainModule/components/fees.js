import React from "react";

const Fees = () => {
		return (
			<main className="down">
				<header className="header-section">
					<div className="header-title">Opłaty</div>
				</header>
				<section className="payment-section">
					<div className="container center">
						<h2>Zapisz się już dzisiaj</h2>
						<div className="payment-option">

							<ul className="payment-header">
								<li className="title">KOSZT UCZESTNICTWA</li>
								<li className="pricing">850zł<span className="one-time"> / jednorazowo</span></li>
								<li>To jedynie 70,84zł na miesiąc</li>
							</ul>

							<p className="line"/>

							<table className="options">
								<tbody>
									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Uczestnictow w konferencji</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Materiały konferencyjne</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Zakwaterowanie</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Pełne wyżywienie</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Publikacje nadesłanej pracy w monografii pokonferencyjenj</span>
										</td>
									</tr>
								</tbody>
							</table>

							<p className="line"/>
							<button type="button" className="btn btn-default button">Zapisz się teraz!</button>

						</div>

						<div className="payment-option">

							<ul className="payment-header">
								<li className="title">KOSZT UCZESTNICTWA</li>
								<li className="pricing">850zł<span className="one-time"> / jednorazowo</span></li>
								<li>To jedynie 70,84zł na miesiąc</li>
							</ul>

							<p className="line"/>

							<table className="options">
								<tbody>
									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Uczestnictow w konferencji</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Materiały konferencyjne</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Zakwaterowanie</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Pełne wyżywienie</span></td>
									</tr>

									<tr>
										<td><img alt="about" className="img-responsive"
											src={require("../assets/photos/arr.png")}/></td>
										<td><span className="program-icon-text">Publikacje nadesłanej pracy w monografii pokonferencyjenj</span>
										</td>
									</tr>
								</tbody>
							</table>

							<p className="line"/>
							<button type="button" className="btn btn-default button">Zapisz się teraz!</button>

						</div>
					</div>
				</section>
			</main>
		);
};

export default Fees;
