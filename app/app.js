/* eslint-disable no-undef */
const express = require("express");
const path = require("path");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const flash = require("connect-flash");
const app = express();
const multer = require("multer");
const cors = require("cors");
const fs = require("fs")
const upload = require("express-fileupload")
// const passport = require("passport");
// const session = require("express-session")

app.use(logger("dev"));
app.use(cookieParser());
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(flash());
app.use(upload());
app.set('view engine', 'pug');

// app.use(session({
//     secret: 'keyboard cat',
//     resave: false,
//     saveUninitialized: true,
//     cookie: { secure: true }
// }))
// app.use(passport.initialize());
// app.use(passport.session());


require("././server/routes/index")(app);

app.get("*", function (request, response) {
    response.sendFile(path.resolve(__dirname, "public", "index.html"));
});

app.use(function (req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

module.exports = app;
