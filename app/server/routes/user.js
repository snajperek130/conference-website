import {UserAPI} from "../../config/api";
import {FileActions, UserActions} from "../../config/actionNames";

const userController = require("../controllers").user;

module.exports = (app) => {
    app.post(UserAPI.getUser, (req, res) => {
        userController.getUserById(req, res)
            .then((user) => {
                res.status(200).send({data: user.dataValues, action: UserActions.GET_USER});
            })
            .catch(() => {
                res.status(400).send(UserActions.UNKNOWN_USER);
            });
    });
    app.post(UserAPI.delete, (req, res) => {
        userController.destroy(req, res);
    });
    app.post(UserAPI.update, (req, res) => {
        userController.updateUser(req, res);
    });
};


