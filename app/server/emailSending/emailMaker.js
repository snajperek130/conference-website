/* eslint-disable no-undef */
import {EmailTemplate} from "email-templates";
import * as nodemailer from "nodemailer";
import * as path from "path";
import * as MailerConfig from "../config/mailerConfig";

const transport = nodemailer.createTransport({
    service: "Mailgun",
    auth: {
        user: MailerConfig.MAILGUN_USER,
        pass: MailerConfig.MAILGUN_PASSWORD
    },
    tls: {
        rejectUnauthorized: false
    },
});

module.exports = {
    sendEmailTemlate(users, templateName) {
        return new Promise((resolve, reject) => {
            loadTemplate(templateName, users).then((results) => {
                return Promise.all(results.map((result) => {
                    sendEmail({
                        to: result.context.email,
                        from: MailerConfig.MAILGUN_USER,
                        subject: result.email.subject,
                        html: result.email.html,
                        text: result.email.text,
                    });
                }));
            }).then(() => {
                resolve();
            }).catch((err) => {
                reject();
            });
        });
    }
};

function sendEmail(obj) {
    return transport.sendMail(obj);
}

function loadTemplate(templateName, contexts) {
    let template = new EmailTemplate(path.join(__dirname, "templates", templateName));
    return Promise.all(contexts.map((context) => {
        return new Promise((resolve, reject) => {
            template.render(context, (err, result) => {
                if (err) reject(err);
                else resolve({
                    email: result,
                    context,
                });
            });
        });
    }));
}



